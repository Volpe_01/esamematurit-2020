"use strict"

let btnAddMemo;
let btnUpdateMemo;
let btnDeleteMemo;
let i = 1;

$(document).ready(function () {
    $.get("/carica",function(data, status){
        console.log(data.length + " memo taken successfully");
        for(let cont= 0; cont< data.length; cont++){
            $("#divIns").after('<div id="' + data[cont].id + '" class="divMemo"><div id="div' + data[cont].id + 'Title" class="divTesto"><h2>' + data[cont].title + '</h2></div><div id= "div' + data[cont].id + 'Text" class="divTesto">' + data[cont].text + '</div><div><button id="btnUpdateMemo'+ data[cont].id +'" class="btnUM" onclick="modifica(' + data[cont].id + ')">Modifica</button><button id="btnDeleteMemo" class="btnDM" onclick="deleteMemo(' + data[cont].id + ')">Elimina</button></div></div>');
            i=data[cont].id;
            i++
        }
    })
});


function addMemo() {
    if ($("#titoloAddMemo").val() != "" && $("#textAddMemo").val() != "") {
        $.post("/post",
            {
                id: i,
                title: $("#titleAddMemo").val(),
                text: $("#textAddMemo").val()
            },
        );
        $("#divIns").after('<div id="' + i + '" class="divMemo"><div id="div' + i + 'Title" class="divTesto"><h2>' + $("#titleAddMemo").val() + '</h2></div><div id= "div' + i + 'Text" class="divTesto">' + $("#textAddMemo").val() + '</div><div><button id="btnUpdateMemo" class="btnUM" onclick="modifica(' + i + ')">Modifica</button><button id="btnDeleteMemo" class="btnDM" onclick="deleteMemo(' + i + ')">Elimina</button></div></div>');
        i++;
    }
}

function modifica(i) {
    $("#"+i).html('<input type="text" placeholder="titolo" id="modificaTitolo" class="divTesto"></input><input type="text" placeholder="testo" id="modificaTesto" class="divTesto"></input><button id="confermaModifiche" class="btnAM" onclick="salvaModifiche(' + i + ')">Conferma</button>');
    console.log(i);
}

function salvaModifiche(i) {
    let titoloModificato = $("#modificaTitolo").val();
    console.log(titoloModificato);
    $("#titleAddMemo").val(titoloModificato);  //input text iniziale del titolo
    let testoModificato = $("#modificaTesto").val();
    $("#textAddMemo").val(testoModificato);   //input text iniziale del testo
    console.log(testoModificato);
    $("#" + i).html('<div id="div' + i + 'Title" class="divTesto"><h2>' + titoloModificato + '</h2></div><div id= "div' + i + 'Text" class="divTesto">' + testoModificato + '</div><div><button id="btnUpdateMemo" class="btnUM" onclick="modifica(' + i + ')">Modifica</button><button id="btnDeleteMemo" class="btnDM" onclick="deleteMemo(' + i + ')">Elimina</button></div></div>');
    $.ajax({
        url: '/put',
        type: 'PUT',
        data: {
            id: i,
            title: titoloModificato,
            text: testoModificato,
        },
        success: function(result) {
            console.log("put avvenuto con successo")
        }
    });
}

function deleteMemo(i){
    $("#" + i).remove();
    $.ajax({
        url: '/delete',
        type: 'DELETE',
        data: {
            id: i,
        },
        success: function(result) {
            console.log("delete avvenuto con successo")
        }
    });
}


