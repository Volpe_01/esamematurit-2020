const express = require('express');
const app = express();
const Sequelize = require('sequelize');

app.set("view engine", "ejs");
app.use(express.urlencoded());
app.use(express.json());

var path = require('path');
app.use(express.static(path.join(__dirname, 'dir_client')));

let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, 
      min: 0, 
      idle: 10000 
    },
});

let Memos = sequelize.define('memo', {
    title: Sequelize.STRING,
    text: Sequelize.STRING,
    preference: Sequelize.ENUM('true','false'),
});
var Tags = sequelize.define('tag', {
    tag: Sequelize.STRING,
});
Memos.hasMany(Tags, { onDelete: 'CASCADE'});
Tags.belongsTo(Memos);

sequelize.sync();

//--------------GET-----------------
app.get("/", function (req, res) {
    console.log("-------------GET");
    res.render('index');
    console.log("pagina caricata");
})

//--------------GET-CARICA-MEMO-----------------
app.get("/carica", function (req, res) {
    console.log("-------------GET CARICA MEMO");
    Memo.findAll().then(memos => {
        res.json(memos);
    })
    console.log("memo caricati");

})

//--------------POST-----------------
app.post("/post", function (req, res) {
    console.log("---------POST");
    let title=  req.body.title;
    let text=req.body.text;
    Memo.create({ title: title, text: text}).then(memo => {
        res.json({ msg : `memo: ${memo} created`});
    })
})
console.log("pronti per il put");

/*sequelize.query('SELECT idMemo FROM Tags JOIN(Memos) ON (Memos.idMemo=Telephones.idMemo) WHERE Memos.title= title AND Memos.text= text').then(([results, metadata]) => {
    let risultati= results;
});*/

//--------------PUT-----------------
app.put("/put", function (req, res) {
    console.log("---------PUT");
    let id = req.body.id;
    let title=  req.body.title;
    let text=req.body.text;
    Memo.update({
        title: title,
        text: text
    },
    {
    where: {
        id: id
    }
    }).then(
        res.json({ msg : 'Memo updated'})
    )

})

//--------------DELETE-----------------
app.delete("/delete", function (req, res) {
    console.log("---------DELETE");
    let id = req.body.id;
    Memo.destroy({
        where: {
                id: id
        }
        }).then(
            res.json({ msg : 'Memo deleted'})
        )   
    
})

app.listen(3000, () => console.log('Server ready'));