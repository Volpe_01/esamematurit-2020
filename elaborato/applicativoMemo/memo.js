module.exports = (sequelize, DataTypes) => {
    const Memo = sequelize.define('memo', {
        title: DataTypes.TEXT,
        text: DataTypes.TEXT
      },
      {
        freezeTableName: true,
      }
    );
    return Memo;
}
