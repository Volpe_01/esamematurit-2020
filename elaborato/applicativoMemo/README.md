# Documentazione memo         			
**DAVIDE VOLPE 5°IB**



questo progetto realizzato durante la quarantena prevede la creazione di un memo con titolo e testo. l'utente può scrivere sull'input text del titolo il relativo titolo, sull'input text del testo scrive il relativo testo, successivamente preme il tasto conferma.
 una volta creati il memo ne può creare altri, modificare memo già esistenti o eliminarli.
 Per creare il codice è stato usato come ide visual studio code, viene utilizzato node con express, viene utilizzata la programmazione asincrona con l'utilizzo di ajax e come base di dati mariadb. per collegare mariadb ad express viene utilizzato sequelize.
 uso css per la grafica.
 
 
 **NODE**
 **Node.js**  è una runtime di JavaScript  [Open source](https://it.wikipedia.org/wiki/Open_source "Open source")  multipiattaforma  [orientato agli eventi](https://it.wikipedia.org/wiki/Programmazione_a_eventi "Programmazione a eventi")  per l'esecuzione di codice  [JavaScript](https://it.wikipedia.org/wiki/JavaScript "JavaScript"), costruita sul  [motore JavaScript V8](https://it.wikipedia.org/wiki/V8_(motore_JavaScript) "V8 (motore JavaScript)")  di  [Google Chrome](https://it.wikipedia.org/wiki/Google_Chrome "Google Chrome"). Molti dei suoi moduli base sono scritti in  [JavaScript](https://it.wikipedia.org/wiki/JavaScript "JavaScript"), e gli sviluppatori possono scrivere nuovi moduli in  [JavaScript](https://it.wikipedia.org/wiki/JavaScript "JavaScript").

In origine JavaScript veniva utilizzato principalmente lato  [client](https://it.wikipedia.org/wiki/Client "Client"). In questo scenario gli script JavaScript, generalmente incorporati all'interno dell'HTML di una pagina web, vengono interpretati da un motore di esecuzione incorporato direttamente all'interno di un  [Browser](https://it.wikipedia.org/wiki/Browser "Browser"). Node.js consente invece di utilizzare JavaScript anche per scrivere codice da eseguire lato server, ad esempio per la produzione del contenuto delle pagine web dinamiche prima che la pagina venga inviata al  [Browser](https://it.wikipedia.org/wiki/Browser "Browser")  dell'utente. Node.js in questo modo permette di implementare il cosiddetto paradigma "JavaScript everywhere" (JavaScript ovunque), unificando lo sviluppo di applicazioni Web intorno ad un unico linguaggio di programmazione (JavaScript).
 sito wikipedia node: [Node]([https://it.wikipedia.org/wiki/Node.js](https://it.wikipedia.org/wiki/Node.js) "node")

**EXPRESS.JS**
**Express**, è un  framework per applicazioni web per  node.js, sotto licenza MIT. È stato progettato per creare  web application e  [API](https://it.wikipedia.org/wiki/Application_programming_interface "Application programming interface") ed ormai definito il server framework standard de facto per  [Node.js](https://it.wikipedia.org/wiki/Node.js "Node.js").

L'autore original, lo descrive come un'infrastruttura di base minimale estendibile con innumerevoli plugin. Express è la parte  [backend](https://it.wikipedia.org/wiki/Front-end_e_back-end "Front-end e back-end")  dello  [stack](https://it.wikipedia.org/wiki/Pila_(informatica) "Pila (informatica)")  [MEAN](https://it.wikipedia.org/w/index.php?title=MEAN&action=edit&redlink=1 "MEAN (la pagina non esiste)"), insieme al database  [MongoDB](https://it.wikipedia.org/wiki/MongoDB "MongoDB")  e al framework frontend  [AngularJS](https://it.wikipedia.org/wiki/AngularJS "AngularJS").
 

**AJAX**
In  informatica **AJAX**,  acronimo  di  **Asynchronous JavaScript and XML**, è una tecnica di sviluppo  software  per la realizzazione di  Applicazione web  interattive Rich Internet Application. Lo sviluppo di applicazioni  [HTML](https://it.wikipedia.org/wiki/HTML "HTML")  con AJAX si basa su uno scambio di dati  in  [background](https://it.wikipedia.org/wiki/Esecuzione_in_background "Esecuzione in background")  fra web browser  e  web server, che consente l'aggiornamento dinamico di una pagina web  senza esplicito ricaricamento da parte dell'utente.

AJAX è  _asincrono_  nel senso che i dati extra sono richiesti al server e caricati in background senza interferire con il comportamento della pagina esistente. Normalmente le funzioni richiamate sono scritte con il linguaggio  [JavaScript](https://it.wikipedia.org/wiki/JavaScript "JavaScript"). Tuttavia, e a dispetto del nome, l'uso di JavaScript e di  [XML](https://it.wikipedia.org/wiki/XML "XML")  non è obbligatorio, come non è detto che le richieste di caricamento debbano essere necessariamente asincrone.

**MARIADB**
**MariaDB** è un [DBMS](https://it.wikipedia.org/wiki/DBMS "DBMS") nato da un fork di [MySQL](https://it.wikipedia.org/wiki/MySQL "MySQL") creato dal programmatore originale di tale programma.
 Aperto ai contributi della comunità, l'area di sviluppo principale è lo [storage engine](https://it.wikipedia.org/wiki/Storage_engine "Storage engine")  [Aria](https://it.wikipedia.org/wiki/Aria_(storage_engine) "Aria (storage engine)"), precedentemente chiamato Maria da cui deriva MariaDB; si tratta di un'evoluzione di [MyISAM](https://it.wikipedia.org/wiki/MyISAM "MyISAM"). Viene dedicata attenzione anche ai [plugin](https://it.wikipedia.org/wiki/Plugin_(informatica) "Plugin (informatica)").

**Difficoltà:**
ho avuto molte difficoltà per capire bene la programmazione asincrona, poi quando ho capito a grandi linee come funziona il tutto sono andato spedito al risultato.
nelle 3 versioni del memo l'ultima è stata la più semplice.
ho perso tanto tempo a correggere il problema che mi faceva modificare solo l'ultimo memo inserito, mentre con gli altri si buggava; alla fine l'errore era che avevo sbagliato con un contatore che mi mandava tutto in pasticcio. 
inoltre ho dovuto pensarci bene prima di fare i get perchè non sapevo come caricare la pagina e i memo allora ne ho creati 2 diversi, prima si carica la pagina poi si carica i memo salvati.
il metodo più semplice è stato il delete in quanto mi è bastato fare un .destroy() sul memo desiderato.

**cosa dovrebbe colpirla del mio progetto?**
la grafica, non è fatta con cose strane tipo bulma ma vedere titolo e testo dentro ad un memo vero e proprio è bello da vedere e ti fa sorridere quindi di sicuro sarà soddisfatto.


cordiali saluti prof morettin.
DV





